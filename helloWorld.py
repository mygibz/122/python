import textwrap

try:
    fh = open("somefile", "r")
    # Wrap this text. 
    wrapper = textwrap.TextWrapper(width=64) 
    
    word_list = wrapper.wrap(text=fh.read())

    # Print each line. 
    for element in word_list: 
        print(element) 
except IOError:
    print "Cannot read somefile"